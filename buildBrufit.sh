#!/bin/bash
set -e
if [ -z "$BRUFIT" ]; then
  echo "ERROR: BRUFIT env var not set"
  exit 1
fi
pushd ${BRUFIT}
mkdir -p build
pushd build
cmake -DCMAKE_C_COMPILER=$(which gcc) ../
make install
cmake -DCMAKE_C_COMPILER=$(which gcc) ../
make install
echo "build finished!"
popd
if [ ! -f "${BRUFIT}/macros/PDFExpand_C.so" ]; then
  echo ">>> now running a brufit tutorial to build remaining shared libraries (e.g. PDFExpand)..."
  pushd tutorials/SphHarmonic
  root -b -q $BRUFIT/macros/LoadBru.C $* GenSphHarmonicMoments.C
  popd
fi
popd
echo "DONE"
