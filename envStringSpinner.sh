#!/bin/bash
# edit stringspinner Makefile for my dependency paths
if [ $# -ne 1 ]; then
  echo "USAGE: $0 [PYTHIADIR]"
  echo "  --->> did you forget to source env.sh?"
  exit 2
fi
sed -i.bak "s;^PYTHIADIR=.*$;PYTHIADIR=$1;" stringspinner/Makefile
grep -E --color '^PYTHIADIR' stringspinner/Makefile
