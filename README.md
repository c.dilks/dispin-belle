# dispin-pythia

Dihadron Partial Wave Analysis using Pythia + StringSpinner

**NOTE**: most of this code has been copied from my CLAS12 analysis
code, [dispin](https://github.com/c-dilks/dispin), and modified to
streamline fitting of data directly from Pythia.

## Dependencies
- ROOT (tested with v6.24/02)
- Pythia (tested with v8.244)
- [StringSpinner](https://gitlab.com/albikerbizi/stringspinner)
- [BruFit](https://github.com/dglazier/brufit)
- Ruby (somewhat optional; tested with v2.7.5)
- Python3

StringSpinner and BruFit are installable as submodules; to obtain
them, clone this repository with `git clone --recurse-submodules`

## Building
1. edit `env.sh`, setting `PYTHIADIR`; if your version of
   StringSpinner or BruFit is not a submodule of this repository, edit
   the corresponding env vars accordingly
1. run `source env.sh`
1. run `make`; this will build BruFit, StringSpinner, and then this
   repository (DiSpin); run `make clean` if you want to try again from
   a clean slate

## Simulation
- run `simulate.exe`
  - run with no arguments for usage guide
  - default output is a `TTree` in `out/`, defined in the `BruTree`
    class (`src/BruTree.*`)
  - use the wrapper `run_simulate.rb` to run `simulate.exe`
    multi-threaded; edit the settings in this script to control how
    many events are run, and how many threads are executed
    simultaneously
    - you will need `ruby` (tested v2.7.5) and the gems in `Gemfile`
      (run `bundle install` to install them)
    - ouptut `TTrees` will be hadded together into one file; logs are
      also written out in the `out/` directory

## Partial Wave Fit
- run `brufit.sh -b -q 'pwFit.C(...)'` or its wrapper `run_pwFit.rb`
  - the wrapper `run_pwFit.rb` is likely user-friendlier
    - configure the settings at the top of the script, in
      particular, the binning schemes and input file
    - the `system` call near the end executes the fit; change
      `system` to `puts` to see what `pwFit.C` calls will be
      executed
  - see `pwFit.C` for documentation of the parameters
  - BruFit can run a maximum likelihood fit using Minuit MIGRAD or
    Markov Chain Monte Carlo (MCMC); use the `minimizer` parameter to
    decide what you want to run
  - multi-threading is handled by PROOF; see `src/BruAsymmetry.cxx` to
    control this behavior
    - run `tailProof.rb` to monitor the PROOF log files during
      execution
    - errors in PROOF logs will be automatically filtered later, by
      `errorPrintProof.rb`, and written to a summary log file in your
      specified output directory
  - most of `pwFit.C` is configuring an instance of `BruAsymmetry`
    - use `BruAsymmetry::AddNumerMod` to add modulations to the fit
      PDF
    - see `src/Modulation.*` for definitions of the modulations and
      partial waves
    - the `Binning` class controls the binning scheme and bin
      boundaries
  - the output will be in several files in your specified output
    directory
    - the output `asym*.root` file will contain the final fit results
    - there is also a log file, which should be checked for any
      errors; some of the logging requires `ruby` to be installed
    - the `BruBin` class provides access to the fit results and
      distributions for each bin
  - if you want to do a weighted likelihood fit (e.g., sWeights), you
    need to add an indexing branch with `indexTree.C`
    - if not, you will get a warning about missing `Idx` (you can
      ignore this if your fit is unweighted)
    - see the original `dispin` repo for sWeight code

### Plotting
- use `pwPlot.py`, or its wrapper `run_pwPlot.rb`, to plot the
  fit results
  - configure settings at the top of `run_pwPlot.rb`
  - run `pwPlot.py` with no arguments for usage guidance
