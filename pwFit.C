R__LOAD_LIBRARY(DiSpin)
#include "BruAsymmetry.h"

// IMPORTANT: run with `brufit.sh -b -q pwFit.C`
/* - minimizer: optimizer algorithm; see src/Constants.h MinimizerStrToEnum
 *              for available minimizer strings
 */

void pwFit(
    TString dataTree="out/run3.hadd.root", // data TTree
    TString mcTree="",                     // MC TTree, for approximating the likelihood normalizer term (leave empty to disable)
    TString bruDir="bruspin/run3.m",       // output directory
    TString minimizer="minuit",            // minimizer (see comments above)
    TString sPlotDir="",                   // sPlot directory (leave empty string if not using, otherwise use outDir from `sPlotBru.C`)
    Int_t   binschemeIVtype=2,             // independent variable ("IV") binning scheme (see script's stdout from `new Binning()`)
    Int_t   nbins0=6,                      // number of bins for each dimension
    Int_t   nbins1=-1,                     // - example: binschemeIVtype=32, nbins0=6, nbins1=3, runs fit in 6 bins of z (iv=3) for 3 bins of Mh (iv=2)
    Int_t   nbins2=-1                      // - leave `-1` to use defaults defined in `src/Binning.cxx`
) {

  // define binning scheme
  Binning * BS = new Binning();
  BS->SetScheme(binschemeIVtype,nbins0,nbins1,nbins2);

  // set PROOF sandbox (where log files etc. are written)
  TString sandbox = TString(gSystem->Getenv("PWD")) + "/" + bruDir + "/prooflog";
  gEnv->SetValue("ProofLite.Sandbox",sandbox.Data());
  printf("proof sandbox = %s\n",gEnv->GetValue("ProofLite.Sandbox","ERROR"));

  // instantiate brufit
  BruAsymmetry * B = new BruAsymmetry(bruDir,minimizer,-1);


  // build modulations -----------------------------------------------------------------------------
  const Int_t Lmin = 0;
  const Int_t Lmax = 2;
  /* // A_UT: all partial waves up to L=Lmax
  for(int T=2; T<=3; T++) {
    int nSF = T==2 ? 3 : 2; // twist 2(3) has 3(2) structure functions
    for(int N=0; N<nSF; N++) {
      // none of the structure functions are *only* sensitive to G_1, so we don't have any symmetry relations and therefore must fit *all* possible partial waves
      for(int L=Lmin; L<=Lmax; L++) {
        for(int M=-L; M<=L; M++) {
          B->AddNumerMod(new Modulation(T,L,M,N,true,Modulation::kUT));
        };
      };
    };
  };
  */
  ///* // A_LU: all 12 partial waves up to L=Lmax
  for(int L=Lmin; L<=Lmax; L++) {
    for(int M=0; M<=L; M++) {
      for(int T=2; T<=3; T++) {
        if(T==2 && M==0) continue; // since G_1^|l,0> == 0
        B->AddNumerMod(new Modulation(T,L,M,0,true,Modulation::kLU));
        if(T==3 && M>0) B->AddNumerMod(new Modulation(T,L,-M,0,true,Modulation::kLU)); // note: G_1^|l,-m> == G_1^|l,+m>
      };
    };
  };
  //*/
  Tools::PrintTitleBox(Form("NUMBER OF FIT PARAMS: %d",B->GetNumMods()));
  printf("\n");

  // build full PDF
  B->BuildPDF();


  // set binning scheme for bruFit ---------------------------------------------------------------------------
  B->Bin(BS);
  B->PrintBinScheme();


  // load data and MC catTrees -------------------------------------------------------------------------------
  if(sPlotDir=="") B->LoadDataSets( dataTree, mcTree  );
  else             B->LoadDataSets( dataTree, mcTree, sPlotDir+"/Tweights.root", "Signal" );


  // MCMC hyperparameters ------------------------------------------------------------------------------------
  // - chain 1
  B->MCMC_iter   = 3000; // number of samples
  B->MCMC_burnin = 0.1 * ((Double_t)B->MCMC_iter); // number to burn
  B->MCMC_norm   = 1.0 / 0.015; // ~ 1/stepsize
  // - chain 2 (for minimizer=="mcmccov")
  B->MCMC_cov_iter   = 15000; // number of samples
  B->MCMC_cov_burnin = 0.1 * ((Double_t)B->MCMC_iter); // number to burn
  B->MCMC_cov_norm   = 1.0 / 0.03; // ~ 1/stepSize
  // - acceptance rate locks
  B->MCMC_lockacc_target = 0.234; // standard "optimal" acceptance rate
  B->MCMC_lockacc_min    = B->MCMC_lockacc_target - 0.02;
  B->MCMC_lockacc_max    = B->MCMC_lockacc_target + 0.02;


  // perform fit -----------------------------------------------------------------------------------------------
  B->Fit();

  // print acceptance rates
  TString cmd;
  cmd = Form(".! ./mcmcAcceptanceRate.rb %s",bruDir.Data());
  printf("\nEXECUTE: %s\n\n",cmd.Data());
  gSystem->RedirectOutput(B->GetLogName());
  gROOT->ProcessLine(cmd.Data());
  gSystem->RedirectOutput(0);

  // print PROOF errors
  cmd = Form(".! ./errorPrintProof.rb %s",bruDir.Data());
  printf("\nEXECUTE: %s\n\n",cmd.Data());
  gSystem->RedirectOutput(B->GetLogName());
  gROOT->ProcessLine(cmd.Data());
  gSystem->RedirectOutput(0);

  // draw residuals and pulls for each bin
  for(int d=0; d<BS->dimensions; d++) {
    cmd = Form(".! ./brufit.sh -b -q 'drawResiduals.C(\"%s\",\"%s\",\"%s\")'",bruDir.Data(),BS->GetIVname(d).Data(),minimizer.Data());
    printf("\nEXECUTE: %s\n\n",cmd.Data());
    gROOT->ProcessLine(cmd);
  };

  // draw asymmetries
  cmd = Form(".! ./brufit.sh -b -q 'drawBru.C(\"%s\",\"%s\")'",bruDir.Data(),minimizer.Data());
  printf("\nEXECUTE: %s\n\n",cmd.Data());
  gROOT->ProcessLine(cmd);
};
