// copied from https://gitlab.com/albikerbizi/stringspinner.git `dis.cpp`,
// and modified for usage in this analysis

# include <vector>

// pythia, stringspinner
#include "Pythia8/Pythia.h"
#include "StringSpinner.h"

// dispin
#include "BruTree.h"
#include "Constants.h"
#include "Tools.h"

using namespace Pythia8;

TString GetModeStr(Int_t mode);

/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////

int main(int argc, char** argv) {

  // arguments
  Int_t mode       = 0;
  TString outFileN = Form("out/%d.root",mode);
  int seed         = -1;
  if(argc==1) {
    cerr << "USAGE: " << argv[0] <<
      " [numEvents]" <<
      " [mode(def=" << mode << ")]" <<
      " [outputFileName(def=" << outFileN << ")]" <<
      " [seed(def=" << seed << ")]" <<
      endl;
    cerr << endl;
    cerr << "MODES: " << endl;
    for(int m=0; m<4; m++) cerr << "  " << m << ": " << GetModeStr(m) << endl;
    cerr << endl;
    cerr << "See also HARD-CODED SETTINGS in the source file" << endl << endl;
    return 2;
  }
  Long64_t nEvent;
  if(argc>1) nEvent   = (Long64_t)strtof(argv[1],NULL);
  if(argc>2) mode     = (Int_t)strtof(argv[2],NULL);
  if(argc>3) outFileN = TString(argv[3]); else outFileN = Form("out/%d.root",mode);
  if(argc>4) seed     = (int)strtof(argv[4],NULL);
  TString modeStr = GetModeStr(mode);
  if(modeStr.Contains("UNKNOWN")) return 1;
  cout << "ARGS: "        << endl;
  cout << "  nEvent   = " << nEvent   << endl;
  cout << "  mode     = " << mode     << ": " << modeStr << endl;
  cout << "  outFileN = " << outFileN << endl;
  cout << "  seed     = " << seed     << endl;

  Pythia pythia;
  Event& event = pythia.event;
  SimpleStringSpinner fhooks(pythia);

  // HARD-CODED SETTINGS ////////////////////////////////////////////
  //// incoming particles
  double eProton   = 0.938; // proton beam energy (or target mass)
  double pElectron = 10.6; // electron beam momentum
  double eElectron = Tools::PMtoE(pElectron,PartMass(kE)); // electron beam energy
  //// phase space
  double Q2min     = 1.0; // minimum Q2
  //// select strings: filter events from the specified string; use {0,0} for no filtering
  // int stringSelection[2] = {0,0}; // disable filter
  int stringSelection[2] = {2,2101}; // u === (ud)_0
  ///////////////////////////////////////////////////////////////////

  // Seed
  pythia.readString("Random:setSeed = on");
  pythia.settings.parm("Random:seed",seed);

  // Set up incoming beams, for frame with unequal beam energies.
  pythia.readString("Beams:frameType = 2");

  // BeamA = lepton
  pythia.readString("Beams:idA = 11");
  pythia.settings.parm("Beams:eA", eElectron);
  // BeamB = proton at rest.
  pythia.readString("Beams:idB = 2212");
  pythia.settings.parm("Beams:eB", eProton);

  // Interaction mechanism.  
  pythia.readString("WeakBosonExchange:ff2ff(t:gmZ) = on");

  // Phase-space cut: minimal Q2 of process.
  pythia.settings.parm("PhaseSpace:Q2Min", Q2min);

  // Go down to low x-Bjorken.
  pythia.readString("PhaseSpace:pTHatMinDiverge = 0.5");
  pythia.readString("PhaseSpace:mHatMin = 0.");

  // Set dipole recoil on. Necessary for DIS + shower.
  pythia.readString("SpaceShower:dipoleRecoil = off");

  // QED radiation off lepton not handled yet by the new procedure.
  pythia.readString("PDF:lepton = off");
  pythia.readString("TimeShower:QEDshowerByL = off");
    
  // Choice of PDF = CTEQ5L LO (pSet=2).
  pythia.readString("PDF:pSet = 2");
  pythia.readString("PDF:pHardSet = 2");
    
  // Switch off resonance decays, ISR, FSR, MPI and Bose-Einstein.
  pythia.readString("ProcessLevel:resonanceDecays = off");
  pythia.readString("PartonLevel:FSRinResonances = off");
  pythia.readString("PartonLevel:FSR = off");
  pythia.readString("PartonLevel:ISR = off");
  pythia.readString("PartonLevel:MPI = off");
  pythia.readString("HadronLevel:BoseEinstein = off");

  // Primordial kT is switched off (temporarily).
  pythia.readString("BeamRemnants:primordialKT = off");
  pythia.readString("BeamRemnants:primordialKTremnant = 0.");
	
  // Switch off hadron decays (temporarily).
  pythia.readString("HadronLevel:Decay= off");
  pythia.readString("111:onMode = off"); // pi0
  pythia.readString("221:onMode = off"); // eta
  pythia.readString("331:onMode = off"); // eta'
  pythia.readString("311:onMode = off"); // K0 decay

  // Switch off automatic event listing in favour of manual.
  pythia.readString("Next:numberShowInfo = 0");
  pythia.readString("Next:numberShowProcess = 0");
  pythia.readString("Next:numberShowEvent = 1");

  // string + 3P0
  // how to handle cases the program cannot handle in
  // its current state:
  // reject  -> if Pythia suggests something we cannot handle,
  //            we reject it.
  // disable -> accept and continue without spin corrections.

  // Treatment of hadrons not included in the 3P0 model.    
  fhooks.setHadronMode(fhooks.reject);

  // Treatment of splittings from remnant side.    
  fhooks.setRemnantMode(fhooks.reject);

  // Settings of string fragmentation parameters.
  pythia.readString("StringZ:aLund = 0.9");
  pythia.readString("StringZ:bLund = 0.5");	
  pythia.readString("StringPT:sigma = 0.38");
  pythia.readString("StringPT:enhancedFraction = 0.");

  // Set the complex mass mu = Re(mu) + iIm(mu).
  double mu[2];
  // Following two lines are std values for mu.
  mu[1] = 0.38;
  mu[0] = sqrt(0.754-mu[1]*mu[1]);
  fhooks.setMu(mu[0],mu[1]);

  // Choose to assign target polarisation.
  Int_t modeSpin;
  Bool_t beamPolarized   = false;
  Bool_t targetPolarized = false;
  Vec4 Sproton, Squark;
  switch(mode) {
    case 0: // UT, spin up
      targetPolarized = true;
      modeSpin = 1;
      Sproton.p(0.0, (double)modeSpin, 0.0, 0.0);
      break;
    case 1: // UT, spin down
      targetPolarized = true;
      modeSpin = -1;
      Sproton.p(0.0, (double)modeSpin, 0.0, 0.0);
      break;
    case 2: // LU, spin +
      beamPolarized = true;
      modeSpin = 1;
      Squark.p(0.0, 0.0, -1.0*(double)modeSpin, 0.0); // minus sign, since quark momentum is reversed after hard scattering
      break;
    case 3: // LU, spin -
      beamPolarized = true;
      modeSpin = -1;
      Squark.p(0.0, 0.0, -1.0*(double)modeSpin, 0.0); // minus sign, since quark momentum is reversed after hard scattering
      break;
  }

  // set polarization hooks
  if(targetPolarized) {
    fhooks.setTargetPol(Sproton);
  }
  if(beamPolarized) {
    std::vector<int> quarkList { 2, 1, -2, -1, 3, -3 };
    for(int q : quarkList) fhooks.setQuarkPol(q,Squark); 
  }
    
  // Initialize.
  pythia.init();
  BruTree *BT = new BruTree(outFileN);
  bool useStringSelection = stringSelection[0]!=0 && stringSelection[1]!=0;

  // Variables for test calculation of the pi+ Collins asymmetry.
  double Acoll[2] = {0.0};
  int Npi(0);
  double DNN(0.0);
  double STav(0.0);
  
  // Begin event loop.
  for (int iEvent = 0; iEvent < nEvent; ++iEvent) {

    if (!pythia.next()) continue;

    // string selection
    if(useStringSelection) {
      if( event[7].id() != stringSelection[0] ||
          event[8].id() != stringSelection[1] ) continue;
    }
    // cout << "string: " << event[7].id() << " === " << event[8].id() << endl;

    DISKinematics dis(event[1].p(), event[5].p(), event[2].p());

    // Get the photon and proton momenta in the GNS frame.
    Vec4 pPhoton = dis.GNS*dis.q;
    Vec4 pProton = dis.GNS*dis.hadin;
		
    // Target polarization vector, degree of transverse polarization and azimuthal angle in GNS.
    Vec4 SprotonGNS = dis.GNS*Sproton;
    double ST = 0.0;
    double phiS = 0.0;
    if(targetPolarized) {
      ST   = SprotonGNS.pT();
      phiS = SprotonGNS.phi();
      if(phiS<0.0) phiS += 2.0 * M_PI;
    }
		
    // List some events.
    if(iEvent<20) event.list();
		
    // Loop inside the event output.
    for (int i = 0; i < event.size(); ++i){
        
      // Hadron momentum in GNS, id and status.
      Vec4 phad = dis.GNS*event[i].p();
      int idHad = event[i].id();
      int statusHad = event[i].status();
      // Hadrons fractional energy, azimuthal angle and transverse momentum squared in GNS.	
      double zh = ( phad * pProton ) / (pProton * pPhoton);
      double phihad = atan2(phad.py(),phad.px());
      if (phihad<0) phihad += 2.*M_PI;
      double pT2 = phad.pT2();

      // Fill test Collins asymmetry for pi+.
      if(targetPolarized) {
        if(idHad==211&&zh>0.2&&sqrt(pT2)>0.1&&statusHad==83) {
          Acoll[0] += 2.0*sin(phihad+phiS-M_PI);
          Acoll[1] += pow(2.0*sin(phihad+phiS-M_PI),2);
          Npi += 1;
          DNN += 2.0*(1.0-dis.y)/(1.0+pow(1.0-dis.y,2));
          STav += ST;
        }
      }
             
    } // End loop on particle within the same event.

    // fill tree
    BT->SetSpin(modeSpin);
    BT->SetPhiS(phiS);
    BT->Fill(event,dis);
  } // End loop on events.

  // Calculate test Collins asymmetry for pi+.
  if(targetPolarized) {
    for(int i=0;i<2;i++) Acoll[i] /= Npi;
    DNN /= Npi;
    STav /= Npi;
    cout << "The average Collins asymmetry for pi+ is: \n";
    cout << Acoll[0]/(DNN*STav) << "+/-" << sqrt((Acoll[1]-pow(Acoll[0],2))/Npi)/(DNN*STav) << endl;
  }

  BT->Write();
  return 0;

} // end main


/////////////////////////////////////////////
/////////////////////////////////////////////
/////////////////////////////////////////////

// run modes, for configuring which asymmetry and which spin to run with
TString GetModeStr(Int_t mode) {
  TString ret;
  switch(mode) {
    case 0: ret="UT, spin up";   break;
    case 1: ret="UT, spin down"; break;
    case 2: ret="LU, spin +";    break;
    case 3: ret="LU, spin -";    break;
    default:
            cerr << "ERROR: unknown mode " << mode << endl;
            ret = "UNKNOWN MODE";
  }
  return ret;
}

